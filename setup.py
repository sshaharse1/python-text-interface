from distutils.core import setup

setup(
    name='python-text-interface',
    version='1.1.0',
    packages=['', 'python_gui', 'python_gui.ui', 'python_gui.utils', 'python_gui.utils.input',
              'python_gui.utils.printer'],
    url='',
    license='',
    author='Shahar Sela',
    author_email='sshaharse1@gmail.com',
    description='A generic python text interface'
)
