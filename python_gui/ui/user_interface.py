from python_gui.utils.input.input import int_input
from python_gui.utils.printer.printer import main_print, error_print, clear_screen
from .__consts__ import *


class UserInterface(object):
    def __init__(self, options):
        """
        User interface for printing and selecting options
        :param options: array with ["option to print", bool "selectable"]
        """
        self.options = options

    def __init__(self, options, selectable):
        """
        Will make the whole options array the given selectable param
        :param options: options to display
        :param selectable: boolean if the options are selectable
        """
        self.options = []
        for option in options:
            self.options.append([option, selectable])

    def _print_options(self):
        """
        Clears the screen and prints the options for the user
        :return: None
        """
        clear_screen()
        current_spot = 1
        for option in self.options:
            if option[SELECTABLE_SPOT]:
                main_print(OPTION_FORMAT.format(spot=current_spot, option=option[OPTION_SPOT]))
            else:
                error_print(OPTION_FORMAT.format(spot=current_spot, option=option[OPTION_SPOT]))
            current_spot += 1

    def _select_option(self):
        """
        Lets the user select and option and returns the selected option
        :return: the selected option
        """
        # To make it base 1 and not base 0
        clear_screen()
        self._print_options()
        selected_option = int_input(print_func=self._print_options) - 1
        while not self._check_if_option_is_valid(selected_option):
            clear_screen()
            self._print_options()
            error_print(INVALID_OPTION_PROMPT)
            selected_option = int_input(print_func=self._print_options) - 1
        return self.options[selected_option][OPTION_SPOT]

    def _check_if_option_is_valid(self, option_number):
        """
        Checks if a specific option number is valid
        :param option_number: option to check
        :return: boolean if is valid or not
        """
        if 0 <= option_number < len(self.options):
            if self.options[option_number][SELECTABLE_SPOT]:
                return True
        return False

    def start_choosing(self):
        """
        Lets the user start choosing options
        :return: chosen option
        """
        return self._select_option()
