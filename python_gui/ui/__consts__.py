OPTION_SPOT = 0
SELECTABLE_SPOT = 1

INVALID_OPTION_PROMPT = "Invalid option!"
OPTION_NOT_SELECTABLE = "Option not selectable!"

OPTION_FORMAT = "{spot}. {option}"
