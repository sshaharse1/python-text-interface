from python_gui.utils.printer.printer import error_print, clear_screen, main_print
from .__consts__ import INVALID_INPUT_PROMPT


def int_input(print_func=None):
    """
    Gets input from user, will make him retry if the input is not an int
    :param print_func: will call function if input is invalid to refresh options
    :return: int input
    """
    while True:
        try:
            return int(input())
        except ValueError:
            if print_func is not None:
                print_func()
            error_print(INVALID_INPUT_PROMPT)


def ask_and_get_answered(question):
    """
    Asks the user a question and returns his answer
    :param question: question to ask the user
    :return: user answer
    """
    main_print(question)
    return input()
