from os import system
from .__consts__ import *

from termcolor import *


def main_print(text):
    """
    Prints the given text in the main color
    :param text: text to print
    :return: None
    """
    cprint(text, MAIN_COLOR)


def error_print(text):
    """
    Prints the given text in the error color
    :param text: text to print
    :return: None
    """
    cprint(text, ERROR_COLOR)


def clear_screen():
    """
    Clears the screen
    :return: None
    """
    system(CLEAR_SCREEN_COMMAND)
